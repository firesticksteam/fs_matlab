% anim_block_translate.m

clear; close all;

% Block specification
Lx = 0.15;
Ly = 0.05;
Lz = 0.30;

% Motion data
t = [0:0.005:1]';                % Time data
r = [0.5*sin(2*pi*t), 0*t, 0*t]; % Position data
A = [0*t, 0*t, 0*t];             % Orientation data (x-y-z Euler angle)

n_time = length(t);

% Compute propagation of vertices and patches
for i_time=1:n_time
    
    R = Euler2R(A(i_time,:));
    VertexData(:,:,i_time) = GeoVerMakeBlock(r(i_time,:),R,[Lx,Ly,Lz]);
    [X,Y,Z] = GeoPatMakeBlock(VertexData(:,:,i_time));
    PatchData_X(:,:,i_time) = X;
    PatchData_Y(:,:,i_time) = Y;
    PatchData_Z(:,:,i_time) = Z;
end

% Draw initial figure
figure(1);
h = patch(PatchData_X(:,:,1),PatchData_Y(:,:,1),PatchData_Z(:,:,1),'y');
set(h,'FaceLighting','phong','EdgeLighting','phong');
set(h,'EraseMode','normal');

% Axes settings
xlabel('x','FontSize',14);
ylabel('y','FontSize',14);
zlabel('z','FontSize',14);
set(gca,'FontSize',14);
axis vis3d equal;
view([-37.5,30]);
camlight;
grid on;
xlim([-0.5,0.5]);
ylim([-0.5,0.5]);
zlim([-0.5,0.5]);

% Animation Loop
for i_time=1:n_time
    
    set(h,'XData',PatchData_X(:,:,i_time));
    set(h,'YData',PatchData_Y(:,:,i_time));
    set(h,'ZData',PatchData_Z(:,:,i_time));
    drawnow;
end