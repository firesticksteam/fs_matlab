function setStickPosition(rot)
global p1 p2 V1 V2;
set(p1,'vertices',V1*rot);
set(p2,'vertices',V2*rot);
drawnow