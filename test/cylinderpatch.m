% get cylinder patch
function h=cylinderpatch()

clear; close all;

% Cylinder specification
r = [0,0,0];       % Reference position
A = [0, pi/2, 0]; % Reference orientation (x-y-z Euler angle)

Radius = 0.05;
Height = 1;
SideCount = 20;

% Euler angle -> Orientation matrix
a1 = A(1);
a2 = A(2);
a3 = A(3);

R1 = [1, 0, 0;
    0, cos(a1), -sin(a1);
    0, sin(a1), cos(a1)];

R2 = [cos(a2), 0, sin(a2);
    0, 1, 0;
    -sin(a2), 0, cos(a2)];

R3 = [cos(a3), -sin(a3), 0;
    sin(a3), cos(a3), 0;
    0, 0, 1];

R = R1*R2*R3;

% Vertices
n_side = SideCount;

for i_ver=1:n_side
    VertexData_0(i_ver,:) = [Radius*cos(2*pi/n_side*i_ver),Radius*sin(2*pi/n_side*i_ver),0];
    VertexData_0(n_side+i_ver,:) = [Radius*cos(2*pi/n_side*i_ver),Radius*sin(2*pi/n_side*i_ver),Height];
end

n_ver = 2*n_side;

for i_ver=1:n_ver
    VertexData(i_ver,:) = r + VertexData_0(i_ver,:)*R';
end

% Side Patches
for i_pat=1:n_side-1
    Index_Patch1(i_pat,:) = [i_pat,i_pat+1,i_pat+1+n_side,i_pat+n_side];
end
Index_Patch1(n_side,:) = [n_side,1,1+n_side,2*n_side];

for i_pat=1:n_side
    
    % Side patches data
    PatchData1_X(:,i_pat) = VertexData(Index_Patch1(i_pat,:),1);
    PatchData1_Y(:,i_pat) = VertexData(Index_Patch1(i_pat,:),2);
    PatchData1_Z(:,i_pat) = VertexData(Index_Patch1(i_pat,:),3);
end


h = patch(PatchData1_X,PatchData1_Y,PatchData1_Z,'blue');
set(h,'FaceLighting','phong','EdgeLighting','phong');
set(h,'EraseMode','normal');
