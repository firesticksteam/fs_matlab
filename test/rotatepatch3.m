
close all;
clear all;
[X,Y,Z] = cylinderpatchdata(0.05,1,[0,pi/2,0],[0,0,0]);
[X2,Y2,Z2]=cylinderpatchdata(0.02,0.15,[0 0 0],[0.1,0,0]);
% compound patch
p1=patch(X,Y,Z,'blue');
p2=patch(X2,Y2,Z2,'red');

set(p1,'FaceLighting','phong','EdgeLighting','phong');
set(p1,'EraseMode','normal');
view(3);
axis([-1 1 -1 1 -1 1]);
xlabel('x');
ylabel('y');
zlabel('z');
grid on

V0 = get(p1,'vertices');
W0= get(p2,'vertices');
beta=0;
while true
    beta=beta+0.01;
    rot=Euler2R([0 beta 0]);
    V=V0*rot; 		
    W=W0*rot;
    set(p1,'vertices',V)
    set(p2,'vertices',W)
    drawnow
end