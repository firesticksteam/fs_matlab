% fsfilter try to implement a filter

function Y = fsfilter(b,a,X)

n=size(b,2)-1;
% init states
s=zeros(1,n);
scale=1/a(1);

% do it for complete signal
Y=[];
for i=1:size(X)
    x=X(i);
    y=(b(1)*x+s(1))*scale;
    for k=1:n-1
        s(k)=b(k+1)*x-a(k+1)*y+s(k+1);
    end
    s(n)=b(n+1)*x-a(n+1)*y;
    Y=[Y;y];
end



