% ellipsoid fit of measurements M
% do it in multiple steps
% - fit data with least-square-sum method
% - remove outliers with distances bigger than a threshold times rms
% fit again with least square method
function [offset gain dist rms]=ellipsoid_fit(M,varargin)

% test
global A B;

if size(M,2)~=3
    disp('wrong dimension');
end


% number of iteration
iter=0;
if nargin>=2
    % outliers thresholds in realtion to standard deviation
    thresh=varargin{1};
    iter=max(size(thresh));
end

if nargin>=3
    % get method
    method=varargin{2};
end
if ~exist('method')
    method='gainoffset';
end


if (strcmp(method,'gainonly'))
    % built the data points for linear fit problem [x^2 y^2 z^2]
    X=[M(:,1).*M(:,1) M(:,2).*M(:,2) M(:,3).*M(:,3)];

else
    % built the data points for linear fit problem [x^2 y^2 z^2 2x 2y 2z]
    X=[M(:,1).*M(:,1) M(:,2).*M(:,2) M(:,3).*M(:,3) 2*M(:,1) 2*M(:,2) 2*M(:,3)];
end
% build the result vector
y=ones(size(X,1),1);
% iterate through thresh, remove outliers and do more steps
for step=0:iter
    % get fit
    coeff=X\y;
    % get calibrated results
    y0=X*coeff;
    % get distance sqares
    d2=(y0-1).^2;
    % get distances
    dist=sqrt(d2);
    % get RMS
    rms=sqrt(mean(d2));
    if step>0
        % remove outliers bigger than thresh*rms
        outl=[];
        for i=1:size(M,1)
            if (dist(i)>rms*thresh(step))
                outl=[outl;i];
            end
        end
        % remove outliers
        X(outl,:)=[];
        y=ones(size(X,1),1);
    end
end

% now re-calculate gains and offsets from coeff

if (strcmp(method,'gainonly'))
    coeff=[coeff;0;0;0];
else
end



% offsets
offset=[coeff(4)/coeff(1) coeff(5)/coeff(2) coeff(6)/coeff(3)];
% "Radius"
R2=1+coeff(4)*coeff(4)/coeff(1)+coeff(5)*coeff(5)/coeff(2)+coeff(6)*coeff(6)/coeff(3);
% gains
g=1/sqrt(R2);
gain=[sqrt(coeff(1)/R2) sqrt(coeff(2)/R2) sqrt(coeff(3)/R2)];