% first attempt for ellipsoid fit of measurements M
function [offset gain]=circle_fit2(M)

if size(M,2)~=3
    disp('wrong dimension');
end


% built the data points for linear fit problem [x^2+y^2+z^2 2*x 2*y 2*z
% ]=1
X=[diag(M*M') 2*M(:,1) 2*M(:,2) 2*M(:,3) ];
y=ones(size(M,1),1);

coeff=X\y;

% now re-calculate gains and offsets from coeff

% offsets
offset=[coeff(2)/coeff(1) coeff(3)/coeff(1) coeff(4)/coeff(1)];
% radius
g=sqrt(coeff(1)*coeff(1)/(coeff(1)+coeff(2:4)'*coeff(2:4)));
% gains
gain=[g g g];