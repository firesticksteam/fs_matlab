% plot unit sphere
[x,y,z] = sphere(50);

c=z*0.5;
surf(x,y,z,c,'FaceAlpha',0.5,'LineStyle','none');
colormap gray;