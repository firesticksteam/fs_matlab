% reduces data by eliminating near neighbours

% min distance
MINDIST=0.2;


% do this by using magnetic distances
m=size(data,1);
% valid data flags, first set is valid
valid=[1];
% last index for valid set of data
iv=1;
di=data(1,7:9);
for i=2:m
    % calculate distance to last valid data
    dist=norm(data(i,7:9)-di);
    if dist<MINDIST
        % not valid
    else
        % valid, mark it and get new set of data
        valid=[valid;i];
        di=data(i,7:9);
    end
end

reduced_data=data(valid,:);

% plot the magnetic data

plot3(data(:,7),data(:,8),data(:,9),'*','LineStyle','none');
axis([-1.2,1.2,-1.2,1.2,-1.2,1.2]*0.5);
axis manual;
hold on;
xlabel('x');
ylabel('y');

% plot reduced data 
if exist('reduced_data','var')
    plot3(reduced_data(:,7),reduced_data(:,8),reduced_data(:,9),'r*','LineStyle','none');
end



% plot a transparent sphere
% plot sphere
RADIUS=0.5;
[x,y,z] = sphere(50);

c=z*0.5;
surf(RADIUS*x,RADIUS*y,RADIUS*z,c,'FaceAlpha',0.5,'LineStyle','none');
colormap gray;


hold off;
