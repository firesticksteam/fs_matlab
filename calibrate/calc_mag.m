% calculates calibrated magnetic data with the given calibration parameters
function mag_calib=calc_mag(mag_data,calpar)
mag_calib=(mag_data+repmat(calpar(1:3),[size(mag_data,1) 1])).*repmat(calpar(4:6),[size(mag_data,1) 1]);