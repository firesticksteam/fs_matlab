% saves measured data
mkdir(settings.MEASDIR,date);
basedir=[settings.MEASDIR '\\' date '\\*.mat'];
[filename,pathname]=uiputfile(basedir,'save measurement data');
save([pathname filename]);