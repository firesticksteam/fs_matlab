% objective function for accleeration
% mean accel length - 1, function to minimize
function dev=accel_obj1(calpar,accel_data)
    % real accel value with offsets (calpar(1:3)) and gain (calpar(4:6))
    a=calc_accel(accel_data,calpar);
    % get sqr(norm(a))
    snorm=diag(a*a');
    % get squared difference from 1
    sdiff=(snorm-1).*(snorm-1);
    % return sum of differences
    dev=sum(sdiff);    
end
