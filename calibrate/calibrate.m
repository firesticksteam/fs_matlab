% sensor Kalibrier-routine
function [bias gain] = calibrate()

addpath([pwd '\..\lib']);

% measurement count
MEASCOUNT=1000;
% scaling for accel data to g (full range +-16g)
ACCELSCALE=1/2048;
% scaling for mag data to G (16 bit measurement, 1LSB=0.15�T)
MAGSCALE=0.0015;


% number of columns to be read from binary file
COLUMNS=11;


% include local directories
settings=local_settings();

% data format of binary file
FORMAT=settings.FSFLOAT;

% base directory
basedir=[settings.MEASDIR '\\*.*'];
[filename,pathname]=uigetfile(basedir,'inertial calibration file');
data=csvread([pathname filename])/MEASCOUNT;
accel=data(:,1:3)*ACCELSCALE;
gyro=data(:,5:7);

% base directory
[filename,pathname]=uigetfile(basedir,'magnet calibration file');
fileID = fopen([pathname filename]);
data=fread(fileID,[COLUMNS,inf],FORMAT)';
fclose(fileID);
mag=data(:,8:10)*MAGSCALE;
% reduce mag sample rate by 10
mag=downsample(mag,10);
plot(mag);

% gyrobias is just the mean gyro value while not moving
gyrobias=[-2*mean(gyro) 1 1 1];
% accelbias from nonlinear unconstrained minimization: length of accel
% vector should be one g
[accelbias devaccel]= fminsearch(@acceldev,[0 0 0 1 1 1]);
accelbias(1:3)=accelbias(1:3)/ACCELSCALE;
% vector should be one (normalized magnetic field)
[magbias devmag]=fminsearch(@magdev,[0 0 0 2 2 2]);
magbias(1:3)=magbias(1:3)/MAGSCALE;


% put all together
bias=[accelbias(1:3) gyrobias(1:3) magbias(1:3)];
bias=round(bias);
gain=[accelbias(4:6) gyrobias(4:6) magbias(4:6)];

% save calibration
basedir=[settings.MEASDIR '\\*.mat'];
[filename,pathname]=uiputfile(basedir,'calibration parameters');
save([pathname filename], 'bias','gain','gyrobias','accelbias','magbias');


% mean accel length - 1, function to minimize
function dev=acceldev(bias)
    count=size(accel,1);
    dev=0;
    for i=1:count
        % real accel value with offsets (bias(1:3)) and gain (bias(4:6))
        a=(accel(i,:)+bias(1:3)).*bias(4:6);
        % length of vector
        len=norm(a);
        dev=dev+(len-1)^2;
    end
end

% mean mag length -1, functiopn to minimize
function dev=magdev(bias)
    count=size(mag,1);
    dev=0;
    for i=1:count
        % real accel value with offsets (bias(1:3)) and gain (bias(4:6))
        m=(mag(i,:)+bias(1:3)).*bias(4:6);
        % length of vector
        len=norm(m);
        dev=dev+(len-1)^2;
    end

end




end





