% reads gyro calibration parameter on stick
if ~connected()
    disp ('port not connected')
    return
end

% read all serial data
insize=stickPort.BytesAvailable;
if insize>0
    fread(stickPort, insize);
end


fprintf(stickPort,'gyro_calpar\n');
gyro_calpar=fscanf(stickPort,'%f',6)'
