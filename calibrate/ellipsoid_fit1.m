% first attempt for ellipsoid fit of measurements M
function [offset gain]=ellipsoid_fit1(M)

if size(M,2)~=3
    disp('wrong dimension');
end


% built the data points for linear fit problem [y^2 z^2 2x 2y 2z 1]
X=[M(:,2).*M(:,2) M(:,3).*M(:,3) 2*M(:,1) 2*M(:,2) 2*M(:,3) ones(size(M,1),1)];
y=-M(:,1).*M(:,1);

coeff=X\y;

% now re-calculate gains and offsets from coeff

% offsets
offset=[coeff(3) coeff(4)/coeff(1) coeff(5)/coeff(2)];
% "Radius"
R2=-coeff(6)+coeff(3)*coeff(3)+coeff(4)*coeff(4)/coeff(1)+coeff(5)*coeff(5)/coeff(2);
% gains
gx=1/sqrt(R2);
gain=[gx sqrt(coeff(1))*gx sqrt(coeff(2))*gx];