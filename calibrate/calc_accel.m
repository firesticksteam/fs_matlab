% calculates calibrated accel data with the given calibration parameters
function accel_calib=calc_accel(accel_data,calpar)
accel_calib=(accel_data+repmat(calpar(1:3),[size(accel_data,1) 1])).*repmat(calpar(4:6),[size(accel_data,1) 1]);