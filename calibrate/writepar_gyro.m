% writes gyro calibration parameter on stick
if ~connected()
    disp ('port not connected')
    return
end

if ~exist('gyro_calpar')
    disp('var gyro_calpar does not exist.');
    return;
end

if isnan(sum(gyro_calpar))
    disp ('var gyro_calpar contains NaNs');
    return;
end


stickCommand=['gyro_calpar'];
for i=1:6
    stickCommand=[stickCommand ' ' num2str(gyro_calpar(i),7)];
end
disp (stickCommand);
disp('sending to stick');
fprintf(stickPort,[stickCommand '\n']);
disp('done');