% ellipsoid fit of measurements M
% here we do a 2-step process:
% - fit data with least-square-sum method
% - remove outliers
% fit again with least square method
function [offset gain]=ellipsoid_fit3(M)

if size(M,2)~=3
    disp('wrong dimension');
end


% built the data points for linear fit problem [x^2 y^2 z^2 2x 2y 2z]
X=[M(:,1).*M(:,1) M(:,2).*M(:,2) M(:,3).*M(:,3) 2*M(:,1) 2*M(:,2) 2*M(:,3)];
y=ones(size(X,1),1);
coeff=X\y;

% calibrated results
y0=X*coeff;
% distances
d=(y0-1).^2;
% remove outliers
outl=[];
for i=1:size(M,1)
    if (d(i)>0.05)
        outl=[outl;i];
    end
end
% remove outliers
X(outl,:)=[];
% do it again
y=ones(size(X,1),1);
coeff=X\y;
% calibrated results
y0=X*coeff;
% distances
d=(y0-1).^2;








% now re-calculate gains and offsets from coeff

% offsets
offset=[coeff(4)/coeff(1) coeff(5)/coeff(2) coeff(6)/coeff(3)];
% "Radius"
R2=1+coeff(4)*coeff(4)/coeff(1)+coeff(5)*coeff(5)/coeff(2)+coeff(6)*coeff(6)/coeff(3);
% gains
g=1/sqrt(R2);
gain=[sqrt(coeff(1))*g sqrt(coeff(2))*g sqrt(coeff(3))*g];