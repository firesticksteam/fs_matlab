% first attempt for ellipsoid fit of measurements M
function [offset gain]=circle_fit1(M)

if size(M,2)~=3
    disp('wrong dimension');
end


% built the data points for linear fit problem [2*x 2*y 2*z
% 1]=-[x^2+y^2+z^2]
X=[2*M(:,1) 2*M(:,2) 2*M(:,3) ones(size(M,1),1)];
y=-[M(:,1).*M(:,1)+M(:,2).*M(:,2)+M(:,3).*M(:,3)];

coeff=X\y;

% now re-calculate gains and offsets from coeff

% offsets
offset=[coeff(1) coeff(2) coeff(3)];
% radius
R=sqrt(coeff(1:3)'*coeff(1:3)-coeff(4));
% gains
gain=[1/R 1/R 1/R];