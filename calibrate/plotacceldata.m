% plot accel data from static_data
static_accel=static_data(:,1:3);
hold off;
plot3(static_accel(:,1),static_accel(:,2),static_accel(:,3),'*','LineStyle','none');
axis([-1.2,1.2,-1.2,1.2,-1.2,1.2]);
axis manual;
hold on;
xlabel('x');
ylabel('y');
grid on;

% plot calibrated data if existant
if exist('static_accel_calib','var')
    plot3(static_accel_calib(:,1),static_accel_calib(:,2),static_accel_calib(:,3),'*r','LineStyle','none');
end

plotunitspehere;