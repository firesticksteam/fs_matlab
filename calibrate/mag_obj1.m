% objective function for magnet data
% mean magnet length - 1, function to minimize
function dev=mag_obj1(calpar,mag_data)
    % real accel value with offsets (calpar(1:3)) and gain (calpar(4:6))
    a=calc_mag(mag_data,calpar);
    % get sqr(norm(a))
    snorm=diag(a*a');
    % get squared difference from 1
    sdiff=(snorm-1).*(snorm-1);
    % return sum of differences
    dev=sum(sdiff);    
end
