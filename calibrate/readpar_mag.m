% reads mag calibration parameter on stick
if ~connected()
    disp ('port not connected')
    return
end


% read all serial data
insize=stickPort.BytesAvailable;
if insize>0
    fread(stickPort, insize);
end

fprintf(stickPort,'mag_calpar\n');

mag_calpar=fscanf(stickPort,'%f',6)'
