function gyro_calpar=calib_gyro(table_data)

% calibrate gyro: get offsets from static measurements, gains from 90�
% movement
% sensor scaling



% offsrgt from staic measurement?
% gyro_offset=round(-2*mean(static_data(:,4:6))/GYROSCALE);

TIMESTEP=0.001;
STD_WINDOW=100;
LP_FREQU=20;
% threshold for detecting movement
MOV_THRESH = 0.2;
% threshold for detecting standard deviation
STD_THRESH = 0.01;
% min duration for static position
MIN_STATIC_TIME=0.5;


calpar_list=[];


% do it for all measured table_data
for measno=1:size(table_data,2)
    close all;
    table_gyro=table_data{measno}(:,4:6);
    % get the calibrated axis
    gyrstd=std(table_gyro);
    % get axis with highest standard deviation
    [maxgyr,axno]=max(gyrstd);
    single_gyro=table_gyro(:,axno);
    % get lowpass filtered data
    [B,A]=butter(2,LP_FREQU*TIMESTEP*2);
    single_gyro_lp=filtfilt(B,A,single_gyro);
    % get all movements when filtered value is over threshold
    moving=1;
    movespan=[];
    movements=[1 1];
    for i=1:size(single_gyro_lp,1)
        if moving
            if abs(single_gyro_lp(i))<MOV_THRESH
                moving=0;
                if ~isempty(movespan)
                    movespan(1,2)=i;
                    movements=[movements;movespan];
                    movespan=[];
                end
            end
        else
            if abs(single_gyro_lp(i))>MOV_THRESH
                % new start of movement detected
                movespan(1,1)=i;
                moving=1;
            end
        end
    end
    movements=[movements;i i];
    isstatic=0;
    staticspan=[];
    staticpos=[];
    % get all static positions when running std is under threshold
    for i=1:size(movements,1)-1
        static_data=single_gyro_lp((movements(i,2):movements(i+1,1)));
        if size(static_data,1)>MIN_STATIC_TIME/TIMESTEP
            static_data_std=movingstd(static_data,STD_WINDOW);
            for k=size(static_data_std,1):-1:1
                if isstatic 
                    if (static_data_std(k)>STD_THRESH) || k==1
                        isstatic=0;
                        if ~isempty(staticspan)
                            staticspan(1,1)=k;
                            staticpos=[staticpos;staticspan+movements(i,2)-1];
                            staticspan=[];
                            break;
                        end
                    end
                else
                    if static_data_std(k)<STD_THRESH
                        staticspan(1,2)=k;
                        isstatic=1;
                    end
                end
            end
        end
    end
    % get all (concatenated) static data, get starting points in the middle
    % of static data blocks
    static_data=[];
    for i=1:size(staticpos,1)
        staticpos(i,3)=mean(staticpos(i,:));
        static_data=[static_data;single_gyro(staticpos(i,1):staticpos(i,2))];
    end
    
    % get offset as mean of all static data
    offset=-mean(static_data);
    
    % get gyro data without offset from first starting pos
    single_gyro_off=single_gyro+offset;
    % get integrated gyro data, integrate from first starting pos, fill
    % start with zeros
    single_gyro_int=[zeros(staticpos(1,1)-1,1);cumsum(single_gyro_off(staticpos(1,1):end,:))*TIMESTEP];
    
    % get mean values for all static positions
    gyro_pos=[];
    for i=1:size(staticpos,1)
        gyro_pos=[gyro_pos;mean(single_gyro_int(staticpos(i,1):staticpos(i,2)))];
    end
    % separate values
    gyro_pos1=gyro_pos(find(abs(gyro_pos)<pi/4));
    gyro_pos2=gyro_pos(find(abs(gyro_pos)>pi/4));
    % get gain
    gain=pi/2/abs(mean(gyro_pos1)-mean(gyro_pos2));
    
    
    subplot(2,1,1);
    hold off;
    plot(single_gyro);
    hold on;
    for i=1:size(movements,1)
        plot([movements(i,1) movements(i,1)],[-0.2 0.2],'g');
        plot([movements(i,2) movements(i,2)],[-0.2 0.2],'g');
    end
    for i=1:size(staticpos,1)
        plot([staticpos(i,1) staticpos(i,1)],[-0.2 0.2],'r');
        plot([staticpos(i,2) staticpos(i,2)],[-0.2 0.2],'r');
    end
    hold off;
    legend('gyroscope raw data');
    xlabel('time/ms');
    ylabel('\omega/rad/s');
    subplot(2,1,2);
    hold off;
    plot(single_gyro_int*180/pi);
    hold on;
    for i=1:size(staticpos,1)
        k=staticpos(i,1);
        y=single_gyro_int(k)*180/pi;
        plot([k k],[y-10 y+10],'r');
        k=staticpos(i,2);
        y=single_gyro_int(k)*180/pi;
        plot([k k],[y-10 y+10],'r');
    end
    hold off; 
    legend('integrated gyro values after calibration');
    xlabel('time/ms');
    ylabel('\alpha/degree');
    
    
    bpressed=[];
    % Create accept button
    acceptbutton=uicontrol('Style', 'pushbutton', 'String', 'Accept',...
    'Position', [10 5 70 25], 'Callback', @acceptpressed',...
    'FontSize',12);
    % Create discard button
    discardbutton=uicontrol('Style', 'pushbutton', 'String', 'Discard',...
    'Position', [90 5 70 25], 'Callback', @discardpressed',...
    'FontSize',12);
    usertext=uicontrol('Style','text','String',['offset ' num2str(offset) ' gain ' num2str(gain)],...
    'Position', [170 5 300 25], 'FontSize',12);    
    drawnow;
    while isempty(bpressed)
        drawnow;
    end
    if bpressed==1
        % accepted
        disp(['accepted offset = ' num2str(offset) ' gain = ' num2str(gain) ' for axis ' num2str(axno)]);
        calpar_list=[calpar_list;axno offset gain];
    else
        % discarded
        disp('result discarded');
    end
    


end % end of for-loop

% construct calpar
gyro_calpar=[NaN NaN NaN NaN NaN NaN];
offset=[0 0 0];
gain=[0 0 0];
count=[0 0 0];
for i=1:size(calpar_list,1)
    offset(calpar_list(i,1))=offset(calpar_list(i,1))+calpar_list(i,2);
    gain(calpar_list(i,1))=gain(calpar_list(i,1))+calpar_list(i,3);
    count(calpar_list(i,1))=count(calpar_list(i,1))+1;
end
for i=1:3
    if count(i)~=0
        gyro_calpar(i)=offset(i)/count(i);
        gyro_calpar(i+3)=gain(i)/count(i);
    end
end

% cancel button onclick
function acceptpressed(hObject, eventdata, handles)
    % disp('accept pressed');
    bpressed=1;
end

function discardpressed(hObject, eventdata, handles)
    % disp('discard pressed');
    bpressed=0;
end
    
end % end of main function




