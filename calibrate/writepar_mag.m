% writes mag calibration parameter on stick
if ~connected()
    disp ('port not connected')
    return
end

if ~exist('mag_calpar')
    disp('var mag_calpar does not exist.');
    return;
end

if isnan(sum(mag_calpar))
    disp ('var mag_calpar contains NaNs');
    return;
end


stickCommand=['mag_calpar'];
for i=1:6
    stickCommand=[stickCommand ' ' num2str(mag_calpar(i),7)];
end
disp (stickCommand);
disp('sending to stick');
fprintf(stickPort,[stickCommand '\n']);
disp('done');