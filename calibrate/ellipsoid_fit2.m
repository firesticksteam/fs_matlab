% first attempt for ellipsoid fit of measurements M
function [offset gain]=ellipsoid_fit2(M)

if size(M,2)~=3
    disp('wrong dimension');
end


% built the data points for linear fit problem [x^2 y^2 z^2 2x 2y 2z]
X=[M(:,1).*M(:,1) M(:,2).*M(:,2) M(:,3).*M(:,3) 2*M(:,1) 2*M(:,2) 2*M(:,3)];
y=ones(size(M,1),1);

coeff=X\y;

% now re-calculate gains and offsets from coeff

% offsets
offset=[coeff(4)/coeff(1) coeff(5)/coeff(2) coeff(6)/coeff(3)];
% "Radius"
R2=1+coeff(4)*coeff(4)/coeff(1)+coeff(5)*coeff(5)/coeff(2)+coeff(6)*coeff(6)/coeff(3);
% gains
g=1/sqrt(R2);
gain=[sqrt(coeff(1))*g sqrt(coeff(2))*g sqrt(coeff(3))*g];