% writes gyro calibration parameter on stick
if ~connected()
    disp ('port not connected')
    return
end

if ~exist('accel_calpar')
    disp('var accel_calpar does not exist.');
    return;
end

if isnan(sum(accel_calpar))
    disp ('var accel_calpar contains NaNs');
    return;
end


stickCommand=['accel_calpar'];
for i=1:6
    stickCommand=[stickCommand ' ' num2str(accel_calpar(i),7)];
end
disp (stickCommand);
disp('sending to stick');
fprintf(stickPort,[stickCommand '\n']);
disp('done');