% connects serial to stick

% clear all;
addpath([pwd '\..\lib']);
closeAllSerialPorts();

% % scaling for accel data to g (full range +-16g)
% ACCELSCALE=1/2048;
% % scaling for gyro data to 1/s (rad per second) full range +-2000dps
% GYROSCALE=2*pi/5904;
% % scaling for mag data to G (16 bit measurement, 1LSB=0.15�T)
% MAGSCALE=0.0015;
% % scaling for mag data to Gauss (16 bit = 4gauss)
% % MAGSCALE=1/16384;
% % time step in sec
% TIMESTEP=0.001;

global stickPort static_data figure8_data;
% include local directories
settings=local_settings();

% open serial port
stickPort=serial(settings.COMPORT);
% set big input buffer size
set(stickPort,'InputBufferSize',1048576);
fopen(stickPort);
% wait
pause(0.2);
% disable output and read all data
fprintf(stickPort,'serialack off\n');
% get available Bytes
avail=get(stickPort,'BytesAvailable');
% read available Bytes
if (avail>0)
    dummybuf=fread(stickPort,avail);
end

% get the scaling parameters
fprintf(stickPort,'getaccelscale\n');
ACCELSCALE=fscanf(stickPort,'%f');
fprintf(stickPort,'getgyroscale\n');
GYROSCALE=fscanf(stickPort,'%f');
fprintf(stickPort,'getmagscale\n');
MAGSCALE=fscanf(stickPort,'%f');
fprintf(stickPort,'getsampleperiod\n');
TIMESTEP=fscanf(stickPort,'%f');