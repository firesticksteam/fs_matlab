% measures static position from stickport
if ~connected()
    disp ('port not connected')
    return
end

% data format of serial data
FORMAT=settings.FSFLOAT;
% number of measurements with 1000kHz
MEASNUM=500;

% start measurement
fprintf(stickPort,['meansize ' num2str(MEASNUM) '\n']);
% get mean output
fprintf(stickPort,['raw_bin 1 ' num2str(MEASNUM) '\n']);
meas_data=fread(stickPort,[11,1],FORMAT)';
% reset mean 
fprintf(stickPort,'meansize 1\n');
% scale the data
% data must be completely uncalibrated
static_accel=meas_data(:,1:3)*ACCELSCALE;
static_gyro=meas_data(:,5:7)*GYROSCALE;
static_mag=meas_data(:,8:10)*MAGSCALE;
static_data=[static_data;static_accel,static_gyro,static_mag];



