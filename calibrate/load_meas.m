% saves measured data
addpath([pwd '\..\lib']);
settings=local_settings();

basedir=[settings.MEASDIR '\\*.mat'];
[filename,pathname]=uigetfile(basedir,'load measurement data');
load([pathname filename]);