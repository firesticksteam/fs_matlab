% calibrate mag: get mag offsets and gains from figure8_data
figure8_mag=figure8_data(:,7:9);
% options = optimset('MaxFunEvals',10000,'MaxIter',10000);
% [mag_calpar mag_dev]= fminsearch(@(calpar) mag_obj1(calpar,figure8_mag),mag_calpar,options);
% mag_offset=mag_calpar(1:3);
% mag_gain=mag_calpar(4:6);
% [center, radii, evecs, v, chi2 ]= ellipsoid_fit( figure8_mag, '0' );
% mag_gain=diag(evecs*diag(1./radii)*evecs');
% mag_offset=-center;
[mag_offset mag_gain]=ellipsoid_fit(figure8_mag,1,'gainoffset');



mag_calpar=[mag_offset mag_gain];


figure8_mag_calib=calc_mag(figure8_mag,mag_calpar);

figure8_mag_length=diag(figure8_mag_calib*figure8_mag_calib');
figure8_mag_dev=std(figure8_mag_length-1)
