% measures figure8 data from stickport
if ~connected()
    disp ('port not connected')
    return
end

% data format of serial data
FORMAT=settings.FSFLOAT;
% number of columns to be read
COLUMNS=11;
% meas period in ms
MEAS_PERIOD=20;
% measurement time in s
MEAS_TIME=20;
%MEAS_TIME=5;
% measurement time
MEAS_NUM=MEAS_TIME*1000/MEAS_PERIOD;

% set timeout
set(stickPort,'Timeout',MEAS_TIME*1.5);
% set measurement period
fprintf(stickPort,['measperiod ',num2str(MEAS_PERIOD) ' \n']);
% set meansize
fprintf(stickPort,'meansize 1\n');
% start measurement
fprintf(stickPort,['raw_bin ' num2str(MEAS_NUM) '\n']);
meas_data=fread(stickPort,[COLUMNS,MEAS_NUM],FORMAT)';
fprintf(stickPort,'stop\n');

% scale the data
% data must be completely uncalibrated
figure8_accel=meas_data(:,1:3)*ACCELSCALE;
figure8_gyro=meas_data(:,5:7)*GYROSCALE;
figure8_mag=meas_data(:,8:10)*MAGSCALE;
figure8_data=[figure8_data;figure8_accel,figure8_gyro,figure8_mag];

% reset measurement period
fprintf(stickPort,['measperiod 1\n']);
% reset meansize
fprintf(stickPort,'meansize 1\n');


% reset timeout
set(stickPort,'Timeout',10);
