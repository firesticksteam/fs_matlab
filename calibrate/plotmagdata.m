% plot magnetic data from figure8_data
figure8_mag=figure8_data(:,7:9);
plot3(figure8_mag(:,1),figure8_mag(:,2),figure8_mag(:,3),'*','LineStyle','none');
axis([-1.2,1.2,-1.2,1.2,-1.2,1.2]);
axis manual;
hold on;
xlabel('x');
ylabel('y');
grid on;

% plot calibrated data if existant
if exist('figure8_mag_calib','var')
    plot3(figure8_mag_calib(:,1),figure8_mag_calib(:,2),figure8_mag_calib(:,3),'r*','LineStyle','none');
end

plotunitspehere;