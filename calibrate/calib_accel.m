% calibrate accel: get accel offsets and gains from static_data
static_accel=static_data(:,1:3);
% accel calibrtion parameter start values
accel_calpar=[0 0 0 1 1 1];
options = optimset('MaxFunEvals',10000,'MaxIter',10000);
[accel_calpar accel_dev]= fminsearch(@(calpar) accel_obj1(calpar,static_accel),accel_calpar,options);
accel_offset=accel_calpar(1:3);
accel_gain=accel_calpar(4:6);
static_accel_calib=calc_accel(static_accel,accel_calpar);