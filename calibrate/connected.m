function c=connected()
global stickPort;
if ~exist('stickPort','var') || isempty (stickPort) || ~isobject(stickPort) || ~strcmp(stickPort.Type,'serial')
    c=0;
    return
end

if ~strcmp(stickPort.Status,'open')
    c=0;
    return
end
c=1;