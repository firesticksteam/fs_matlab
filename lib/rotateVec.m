% rotates vector with given quaternion
function vec=rotateVec(vec0,q)
vec0=[0 vec0];
vec=quaternProd(q,quaternProd(vec0,quaternConj(q)));
vec=vec(2:4);