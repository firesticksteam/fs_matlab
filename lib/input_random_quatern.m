% create random  quaternion data
function input_quatern_bin()
global inputdata convertfcn;
%t=toc;
%tic;
% disp(['dt= ' num2str(t)]);
alpha=rand(1);
inputdata=[cos(alpha) 0 sin(alpha) 0 0];
convertfcn(inputdata);