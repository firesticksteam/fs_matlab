% starts sertial communication with stick
addpath([pwd '\..\lib']);
closeAllSerialPorts();
global stickPort;
% include local directories
settings=local_settings();

% open serial port
stickPort=serial(settings.COMPORT);
set(stickPort,'InputBufferSize',16384);
fopen(stickPort);
% wait
pause(0.5);
% read all serial data
insize=stickPort.BytesAvailable;
if insize>0
    fread(stickPort, insize);
end

