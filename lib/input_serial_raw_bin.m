% input raw data from serial port binary
function input_serial_raw_bin()
global inputdata convertfcn port;

% data format of serial data
FORMAT='float32';

t=toc;
tic;
% get madgwick data from stick
fprintf(port,'output_binary 9\n');
inputdata=fread(port,[9,1],FORMAT)';
inputdata=[inputdata t];
% disp(inputdata);
convertfcn(inputdata);