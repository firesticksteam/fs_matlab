% input quaternion data from serial port binary
function input_serial_quatern_bin()
global inputdata convertfcn stickPort;

% data format of serial data
FORMAT='float32';


t=toc;
tic;
% get madgwick data from stick
fprintf(stickPort,'madgwick_bin 1\n');
inputdata=fread(stickPort,[4,1],FORMAT)';
inputdata=[inputdata t];
% disp(inputdata);
convertfcn(inputdata);