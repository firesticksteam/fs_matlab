% test the filter algorithm
[b,a]=butter(6,0.05);
signal=ones(100,1);

mfilt=filter(b,a,signal);
ofilt=fsfilter(b,a,signal);

plot([mfilt ofilt]);
legend('matlab filter','own filter');

