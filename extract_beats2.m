% extract beats from data
% measured data: madgwick and calib

% velocity threshold for triggering signal
VELTHRESHUP=1;
VELTHRESHDOWN=1;
% low pass filter frequency
LPFREQU=100;
% relative velocity for detecting max vel
RELVEL1=0.9;
% next point
RELVEL2=0.5;




madgwick_data=data(:,1:4);
accel_data=data(:,5:7);
gyro_data=data(:,8:10);
mag_data=data(:,11:13);

ds=size(data,1);

% reference quaternion
madgwick0=quaternConj(madgwick_data(1,:));
% get quaternions relative to reference, rotated [1 0 0] vector, horizontal
% angle alpha
qr=[];
xrot=[];
alpha=[];
for i=1:ds
    qr(i,:)=quaternProd(madgwick_data(i,:),madgwick0);
    xrot(i,:)=rotateVec([1 0 0],qr(i,:));
    alpha(i,1)=atan2(xrot(i,2),xrot(i,1));
end


% for further processing, get a one dimensional sinal; here gyroy
velocity=gyro_data(:,2);
% filter the signal
[b,a]=butter(2,LPFREQU/500);
velocity_tp=filter(b,a,velocity);

% velocity derivation
angularacc=diff3(velocity_tp);
angularacc(end)=angularacc(end-1);
angularacc(1)=angularacc(2);


maxvel=0;
maxveli=0;
middlevel=0;
middleveli=0;
stopaccel=0;
stopacceli=0;
triggerpoints=[];
lastv=velocity_tp(1);
searchmin=0;
stopdetected=0;
lasta=angularacc(1);



% step by step extraction

for i=1:ds
    v=velocity_tp(i);
    a=angularacc(i);
    if (~maxveli) && (v>VELTHRESHUP) && (v>lastv)   
        maxvel=v;
    end
    % detect peak 1
    if (~maxveli) && (maxvel~=0)
        if v<RELVEL1*maxvel
            maxveli=i;
        end
    end
    % detect peak 2
    if (~middleveli) && maxveli
        if v<RELVEL2*maxvel
            middleveli=i;
            middlevel=v;
        end
    end
    % detect max accel
    if (~stopacceli) && maxveli && (a>lasta)
        stopaccel=a;
        stopacceli=i;
    end    
    % search for velocity turning point
    if(maxveli~=0) && (v<VELTHRESHDOWN) && (v>lastv)
        % this is the trigger point
        triggerpoints=[triggerpoints;maxveli,maxvel,middleveli,middlevel,stopacceli,stopaccel,i,v];
        % enable new measurement
        maxvel=0;
        maxveli=0;
        stopacceli=0;
        stopaccel=0;
        middlevel=0;
        middleveli=0;

    end  
    lastv=v;
    lasta=a;
end

% plot it
close all;
subplot(2,1,1);
plot([velocity_tp velocity angularacc*20]);
hold on;
for i=1:size(triggerpoints,1)
    t=triggerpoints(i,:);
    plot([t(1) t(1)],[t(2)-1 t(2)+1],'r');
    plot([t(3) t(3)],[t(2)-1 t(2)+1],'r');
    plot([t(7) t(7)],[t(8)-1 t(8)+1],'g');
    plot([t(5) t(5)],[t(6)*20-1 t(6)*20+1],'b');
end
hold off;
ylabel('velocity');
subplot(2,1,2);
plot(alpha);
hold on;
for i=1:size(triggerpoints,1)
    t=triggerpoints(i,:);
    plot([t(7) t(7)],[alpha(t(5))-0.2 alpha(t(5))+0.2],'g');
end
hold off;
ylabel('heading');

maxvels=triggerpoints(:,2);
stopaccels=triggerpoints(:,6);