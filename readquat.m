% reads quaternions from log file
% needs some folder definitions:
% MEASDIR : local folder where measurement data is stored



% number of columns to be read
COLUMNS=4;
% data format to be read
FORMAT='float32';

% set local folders
folders=local_folders();
if exist('filename')
    fileID=fopen([folders.MEASDIR '\\' filename],'raw data');
else    
    % base directory
    basedir=[folders.MEASDIR '\\*.*'];
    [filename,pathname]=uigetfile(basedir);
    fileID = fopen([pathname filename]);
end
clear filename;
position=fread(fileID,[COLUMNS,inf],FORMAT)';
fclose(fileID);
