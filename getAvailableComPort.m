function lCOM_Port = getAvailableComPort()
% function lCOM_Port = getAvailableComPort()
% Return a Cell Array of COM port names available on your computer

k=1;
for i=1:100
portname=['COM' int2str(i)];
    
    try
        s=serial(portname);
        fopen(s);
        fclose(s);
        lCOM_Port{k}=portname;
    catch
        lErrMsg = lasterr;
    end
end

