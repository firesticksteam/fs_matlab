% converts raw data to calibrated data

% number of columns to be read
COLUMNS=11;
% data format to be read
FORMAT='int16';
% scaling for accel data to g (full range +-16g)
ACCELSCALE=1/2048;
% scaling for gyro data to 1/s (rad per second) full range +-2000dps
GYROSCALE=2*pi/5904;
% scaling for mag data to G (16 bit measurement, 1LSB=0.15�T)
MAGSCALE=0.0015;
% time step in sec
TIMESTEP=0.001;

% set local folders
folders=local_folders();
% base directory
basedir=[folders.MEASDIR '\\*.*'];
[rawdatafile,pathname]=uigetfile(basedir,'raw data');
rawdatafile=[pathname rawdatafile];
fileID = fopen(rawdatafile);
data=fread(fileID,[COLUMNS,inf],FORMAT)';
fclose(fileID);

% get the calibration parameters
basedir=[folders.MEASDIR '\\*.mat'];
[calibname,pathname]=uigetfile(basedir,'calibration parameters');
load([pathname calibname]);

% data must be completely uncalibrated
accel=data(:,1:3);
Accelerometer=(accel+ ones(size(accel))*diag(accelbias(1:3))).*(ones(size(accel))*diag(accelbias(4:6)))*ACCELSCALE;
gyro=data(:,5:7);
Gyroscope=(gyro+ ones(size(gyro))*diag(gyrobias(1:3))).*(ones(size(gyro))*diag(gyrobias(4:6)/2))*GYROSCALE;
mag=data(:,8:10);
Magnetometer=(mag+ ones(size(mag))*diag(magbias(1:3))).*(ones(size(mag))*diag(magbias(4:6)))*MAGSCALE;
time=(1:size(data,1))'*TIMESTEP;
