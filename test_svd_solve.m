addpath([pwd '.\lib']);
addpath([pwd '.\calibrate']);
addpath([pwd '.\qr_solve']);

settings=local_settings();

% load test data
load([settings.MEASDIR '\\18-Jan-2018\\mag_test_data_ab.mat']);

disp('A\B=');
disp(A\B);
disp('svd_solve(A,B)=');
disp(svd_solve(size(A,1),size(A,2),A,B));