% first attempt for simple beat detection

% threshold dgy
threshdgy=-0.03;
% velocity gain
velgain=-2000;


filename='simpel_001_2017-10-31_18-35-29.log';
readlog;

% init
gyl=0;

% step throug all data
for i=1:size(data,1)
   % gyro y
   gy=gyro(i,2);
   % mag x
   mx=mag(i,1);
   % mag y
   my=mag(i,2);
   % time
   t=i*0.001;
   % diff
   dgy=gy-gyl;
   % detect gy zero crossing, if dgy is under threshold
   if (dgy<threshdgy)
       if gyl>0 && gy<=0
           vel=(dgy-threshdgy)*velgain;
           angle=atan2(mx,my)*180/pi;
           disp(['beat at ' num2str(t) ' seconds: ' num2str(vel) ' ' num2str(angle) '�']);
       end
   end
   
   
   
   % set for next step
   gyl=gy;
   
end