% visualize position

clear; clc; close all 

addpath('quaternion_library'); 

% set local folders
folders=local_folders();
% base directory
basedir=[folders.MEASDIR '\\*.*'];

% get the saved position data
basedir=[folders.MEASDIR '\\*.mat'];
[dataname,pathname]=uigetfile(basedir,'position data');
load([pathname dataname]);

%Create an axes and adjust the view. Set the axes limits to prevent auto limit selection during scaling.
ax = axes('XLim',[-1 1],'YLim',[-1 1],'ZLim',[-1 1]);
view(3);
grid on;
xlabel('x'); ylabel('y'); zlabel('z');
set(gca,'Xtick',[-1 0 1]);
set(gca,'Ytick',[-1 0 1]);
set(gca,'Ztick',[-1 0 1]);
set(gca,'Box','on');
set(gcf, 'Position', [10, 100, 1200, 900])

% construct a cylinder
[X,Y,Z]=cylinder(0.05);
C=(1:size(X,2))/size(X,2);
colormap(jet);
% create surface object, more h(i) elements can bee added here,
% tip of stick point to positive x
h(1) = surface(Z,X,Y,C);
% Create a transform object and parent the surface objects to it. Initialize the rotation and scaling matrix to the identity matrix (eye).
transform = hgtransform('Parent',ax);
set(h,'Parent',transform);

% initialize rotation matrix
rot0 = makehgtform('yrotate',0);
set(transform,'Matrix',rot0);

Rz = eye(4);
Sxy = Rz;

% go through all position data

for i=1:5:size(position,1)
    % get one position quaternion, use conjugate for sensor frame relative
    % to Earth
    q=quaternConj(position(i,:));
    % get rotation matrix
    rot=quatern2rotMat(q);
    r=rot0;
    r(1:3,1:3)=rot*r(1:3,1:3);
    % set the transform Matrix property
    set(transform,'Matrix',r);
    drawnow;
end

pause(1);

% Reset to the original orientation and size using the identity matrix.

set(transform,'Matrix',rot0);