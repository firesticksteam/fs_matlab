% table measurement from stickport
if ~connected()
    disp ('port not connected')
    return
end

% data format of serial data
FORMAT=settings.FSFLOAT;
% number of columns to be read
COLUMNS=11;
% meas period in ms
MEAS_PERIOD=1;

% set measurement period
fprintf(stickPort,['measperiod ',num2str(MEAS_PERIOD) ' \n']);
% set meansize
fprintf(stickPort,'meansize 1\n');



% loop measurement
while(1)
    fprintf(stickPort,['madgwick_bin 1 \n']);
    q=fread(stickPort,[4,1],FORMAT)';
    % rotate x-axis around quaternion
    x=rotateVec([1 0 0],q);
    disp(x);
    pause(0.2);
end


