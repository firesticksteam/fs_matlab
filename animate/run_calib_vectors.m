% animate vectors for accel, gyro, magnet from calibrated sensor data
clear all;
addpath([pwd '\..\lib']);
closeAllSerialPorts();
global inputdata convertfcn vecdraw vecs newdata animationtimer statusline port animrunning;
% include local directories
settings=local_settings();



% the input function
INPUTFCN='input_serial_calib_bin';
% the convert function
convertfcn=@convert_vecs_vecs;
input_data=[];
% create the vector drawing with 3 vecors
vecdraw=createVectorDrawing(3);

% open serial port
port=serial(settings.COMPORT);
fopen(port);
% wait
pause(0.5);
% start measurement
fprintf(port,'run_calib\n');


% start timer
animationtimer = timer('TimerFcn',INPUTFCN, 'Period', 0.1,'ExecutionMode','fixedRate');
start(animationtimer);



animrunning=1;
while animrunning~=0
    if (newdata~=0)

        setVectors(vecdraw,vecs);
        newdata=0;
        %set(statusline(1),'String',[num2str(toc*1000) 'ms']);
        %set(statusline(2), 'String',['mean ' num2str(1000*meant) 'ms']);
    end
    pause(0.1);
end
