
% connect to stick
connect2stick;


% configure oscisettings
% titles for subplots, the number is also the number of subplots
os.titles={'Accel','Gyro','Mag'};
% x data number
os.xnum=300;
% plotted lines per subplot
os.dimensions=[3 3 3];
% y ranges for each subplot
os.yranges=[-4000 4000;-4000 4000;-8000 8000];




% start measurement
oscidraw=createOsciDrawing(os);
pos=1;
while exist('running','dir')
    % get data
    fprintf(stickPort,'raw_bin 1\n');
    meas_data=fread(stickPort,[11,1],settings.FSFLOAT)';
    osci_data=[meas_data(1:3) meas_data(5:7) meas_data(8:10)];
    % set osci data
    setOsciData(oscidraw,pos,osci_data)
    pos=pos+1;
    if (pos>os.xnum) 
        pos=1;
    end
    drawnow;
end




