function stop_animation_timer(source,event)
global animationtimer animrunning;
% stop animation loop
animrunning=0;
% stop timer
if exist('animationtimer','var') && ~isempty(animationtimer)
    stop(animationtimer)
end
delete(timerfind);