
% connect to stick
connect2stick;


% configure oscisettings
% titles for subplots, the number is also the number of subplots
os.titles={'Accel','Gyro','Mag'};
% x data number
os.xnum=300;
% plotted lines per subplot
os.dimensions=[3 3 3];
% y ranges for each subplot
os.yranges=[-1.5 1.5;-1.5 1.5;-1.5 1.5];




% start measurement
oscidraw=createOsciDrawing(os);
pos=1;
while exist('running','dir')
    % get data
    fprintf(stickPort,'calib_bin 1\n');
    meas_data=fread(stickPort,[9,1],settings.FSFLOAT)';
    % set osci data
    setOsciData(oscidraw,pos,meas_data)
    pos=pos+1;
    if (pos>os.xnum) 
        pos=1;
    end
    drawnow;
end




