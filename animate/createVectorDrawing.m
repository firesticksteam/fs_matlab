% create vector drawing for vecnum vectors
function vecdraw=createVectorDrawing(vecnum)

% change the colors here!
colors={'blue','green','red','blue','green','red','blue','green','red'};

close all;
vecdraw.axisXY=subplot(2,2,1);
axis([-2,2,-2,2]);
axis manual;
hold on;
xlabel('x');
ylabel('y');
grid on;
vecdraw.axisXZ=subplot(2,2,2);
axis([-2,2,-2,2]);
axis manual;
hold on;
xlabel('x');
ylabel('z');
grid on;
vecdraw.axisYZ=subplot(2,2,3);
axis([-2,2,-2,2]);
axis manual;
hold on;
xlabel('y');
ylabel('z');
grid on;
vecdraw.axis3D=subplot(2,2,4);
axis([-2,2,-2,2,-2,2]);
axis manual;
hold on;
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

% plot dummy data
for i=1:vecnum
    % create a dummy vector
    alpha=i*pi/18;
    X=cos(alpha);
    Y=sin(alpha);
    Z=sin(alpha);
    vecdraw.vecXY{i}.plot=plot(vecdraw.axisXY,[0 X],[0 Y],'LineWidth',2,'Color',colors{i});
    vecdraw.vecXZ{i}.plot=plot(vecdraw.axisXZ,[0 X],[0 Z],'LineWidth',2,'Color',colors{i});
    vecdraw.vecYZ{i}.plot=plot(vecdraw.axisYZ,[0 Y],[0 Z],'LineWidth',2,'Color',colors{i});
    vecdraw.vec3D{i}.plot=plot3(vecdraw.axis3D,[0 X],[0 Y],[0 Z],'LineWidth',2,'Color',colors{i});
end

% Create push button
btn = uicontrol('Style', 'pushbutton', 'String', 'stop',...
        'Position', [10 20 50 20],...
        'Callback', @stop_animation_timer);  



% testdata
% vec=[1 1 1;0 1 0;1 0 1];
% setVectors(vecdraw,vec);

