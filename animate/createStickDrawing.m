% number of statuslines
STATUSLINECOUNT=3;
% width of one statusline
STATUSLINEWIDTH=150;


addpath([pwd '\..\lib']);
close all;
% clear all;
global p1 p2 V1 V2;
[X,Y,Z] = cylinderpatchdata(0.05,1,[0,pi/2,0],[0,0,0]);
[X2,Y2,Z2]=cylinderpatchdata(0.02,0.15,[0 0 0],[0.1,0,0]);
% compound patch
p1=patch(X,Y,Z,'blue');
p2=patch(X2,Y2,Z2,'red');

set(p1,'FaceLighting','phong','EdgeLighting','phong');
set(p1,'EraseMode','normal');
view(3);
axis([-1 1 -1 1 -1 1]);
xlabel('x');
ylabel('y');
zlabel('z');
grid on

V1 = get(p1,'vertices');
V2= get(p2,'vertices');

% Create push button
btn = uicontrol('Style', 'pushbutton', 'String', 'stop',...
        'Position', [10 20 50 20],...
        'Callback', @stop_animation_timer);  
% create status lines
for i=1:STATUSLINECOUNT
    x=(i-1)*(STATUSLINEWIDTH+10);
    statusline(i)=uicontrol('Style','text','String','..','Position',[x 0 150 20],'HorizontalAlignment','left');
end
    

