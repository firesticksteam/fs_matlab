% starts madgwick & gyro measurement

% number of output data
COLUMNS=7;
% data format of serial data
FORMAT='float32';

start_stick_serial;

% start measurement
fprintf(stickPort,'run_madgwick_gyro\n');
% start output
fprintf(stickPort,'output_binary 7 1');

