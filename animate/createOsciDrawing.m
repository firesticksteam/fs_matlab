% create direction osci drawing for calibrated accel, gyro, mag data
function oscidraw=createOsciDrawing(oscisettings)

% change the colors here!
colors={'blue','green','red','blue','green','red','blue','green','red'};

close all;

% number of subplots
subplotnum=size(oscisettings.titles,2);

for i=1:subplotnum
    oscidraw.axis(i)=subplot(subplotnum,1,i);
    
end


% number of x values
xnum=oscisettings.xnum;

dummydata=zeros(xnum,1);

linenum=0;
for i=1:subplotnum
    hold off;
    a=oscidraw.axis(i);
    % set current axis
    axes(a);
    % plot dummy data
    
    %dirdraw.pointer(i)=polar(a,[0 0],[-1 1]);
    %set(dirdraw.pointer(i),'LineWidth',4,'Color','blue');
    for k=1:oscisettings.dimensions(i)
        linenum=linenum+1;
        oscidraw.line(linenum)=plot(dummydata,'Color',colors{k});
        hold on;
    end
    title(oscisettings.titles{i});
    axis([0,xnum,oscisettings.yranges(i,:)]);
    axis manual;
    grid on;    
    set(a,'GridLineStyle',':');
end



% figure double width
p=get(gcf,'Position');
p(3)=2*p(3);
set(gcf,'Position',p);


% Create cancel button
oscidraw.cancelbutton=uicontrol('Style', 'pushbutton', 'String', 'Cancel',...
    'Position', [10 10 90 40], 'Callback', @cancelpressed',...
    'FontSize',15);
drawnow;
mkdir('.','running');


% create value fields
for i=1:linenum
    oscidraw.valuefield(i)=uicontrol('Style','text','String','0',...
        'Position',[10 70+30*(linenum-i) 90 25], 'FontSize', 12);
end
    
end
% create 

% cancel button onclick
function cancelpressed(hObject, eventdata, handles)
    try
        rmdir('running');
    catch
    end
    disp('function canceled');
end


