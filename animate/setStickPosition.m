% sets stick position from a position quaternion

function setStickPosition(pq)
global p1 p2 V1 V2;
rot=quatern2rotMat(pq);
set(p1,'vertices',V1*rot);
set(p2,'vertices',V2*rot);
drawnow
