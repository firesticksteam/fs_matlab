% measures madgwick and gyro and displays result
addpath([pwd '\..\lib']);
start_madgwick_gyro;
cancelbutton=drawcancelbutton;
while exist('running','dir')
    drawnow;
    meas_data=fread(stickPort,[COLUMNS,100],FORMAT)';
    % get last quternion
    q=meas_data(end,1:4);
    % calculate rotated x-axis
    vx=rotateVec([1 0 0],q);
    % angle to north
    alpha=atan2(vx(1),vx(2))*180/pi;
    disp([vx alpha]);
end
% stop output
fprintf(stickPort,'stop');




