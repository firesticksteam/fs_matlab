% get serial madgwick data from stick and animate it
global inputdata convertfcn posq newdata animationtimer statusline;

% the input function
%INPUTFCN='input_serial_quatern_bin';
INPUTFCN='input_serial_quatern_bin';
% the convert function
convertfcn=@convert_quat_quat;

% connect to stick
connect2stick;

inputdata=[];
% create the drawing
createStickDrawing;

% restart sensor fusion
% fprintf(stickPort,'sensfus_start\n');

% start timer
animationtimer = timer('TimerFcn',INPUTFCN, 'Period', 0.1,'ExecutionMode','fixedRate');
meant=0;
start(animationtimer);
while isvalid(animationtimer)
    if (newdata~=0)
        tic
        setStickPosition(posq);
        newdata=0;
        t=toc;
        meant=(meant*99/100+t/100);
        set(statusline(1),'String',[num2str(toc*1000) 'ms']);
        set(statusline(2), 'String',['mean ' num2str(1000*meant) 'ms']);
    end
end
