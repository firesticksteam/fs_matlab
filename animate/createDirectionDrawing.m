% create direction drawing for horizontal and vertical direction
function dirdraw=createDirectionDrawing()

% change the colors here!
colors={'blue','green','red','blue','green','red','blue','green','red'};

close all;
dirdraw.axisHor=subplot(1,2,1);
title('Hor');

dirdraw.axisVer=subplot(1,2,2);
title('Ver');

allAxes=[dirdraw.axisHor dirdraw.axisVer];
for i=1:max(size(allAxes))

    a=allAxes(i);
    % set current axis
    axes(a);
    % plot dummy data
    dirdraw.pointer(i)=polar(a,[0 0],[-1 1]);
    set(dirdraw.pointer(i),'LineWidth',4,'Color','blue');

    axis([-1,1,-1,1]);
    axis manual;
    hold on;
    grid on;    
    set(a,'GridLineStyle',':','PlotBoxAspectRatio',[1 1 1],'LineWidth',2,'Color','blue');

end

% figure double width
p=get(gcf,'Position');
p(3)=2*p(3);
set(gcf,'Position',p);


% Create cancel button
cancelbutton=uicontrol('Style', 'pushbutton', 'String', 'Cancel',...
    'Position', [10 10 90 40], 'Callback', @cancelpressed',...
    'FontSize',15);
drawnow;
mkdir('.','running');
end

% cancel button onclick
function cancelpressed(hObject, eventdata, handles)
    try
        rmdir('running');
    catch
    end
    disp('function canceled');
end


