% sets osci data for all oscis
function setOsciData(oscidraw,pos,data)
linenum=size(oscidraw.line,2);
for i=1:linenum
    gdata=get(oscidraw.line(i),'YData');
    gdata(pos)=data(i);
    set(oscidraw.line(i),'YData',gdata);
    set(oscidraw.valuefield(i),'String',num2str(data(i)));
end


end

