function cancelbutton=drawcancelbutton()
try
    close cancelwindow;
catch
end
cancelwindow=figure('MenuBar','none','Name','cancelwindow');
pos=get(cancelwindow,'Position');
pos(3)=200;
pos(4)=100;
set(cancelwindow,'Position',pos);
cancelbutton=uicontrol('Style', 'pushbutton', 'String', 'Cancel',...
    'Position', [10 10 180 80], 'Callback', @cancelpressed',...
    'FontSize',30);
drawnow;
mkdir('.','running');





end
function cancelpressed(hObject, eventdata, handles)
    rmdir('running');
    disp('function canceled');
end