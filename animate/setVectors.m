% sets 3D vectors in vectorDrawing
function setVectors(vecdraw,vectors)


% get minimal size
s=size(vecdraw.vecXY,2);
s1=size(vectors,1);
if s1<s
    s=s1;
end

% exchange data
for i=1:s
    set(vecdraw.vecXY{i}.plot,'XData',[0 vectors(i,1)]);
    set(vecdraw.vecXY{i}.plot,'YData',[0 vectors(i,2)]);
    set(vecdraw.vecXZ{i}.plot,'XData',[0 vectors(i,1)]);
    set(vecdraw.vecXZ{i}.plot,'YData',[0 vectors(i,3)]);
    set(vecdraw.vecYZ{i}.plot,'XData',[0 vectors(i,2)]);
    set(vecdraw.vecYZ{i}.plot,'YData',[0 vectors(i,3)]);
    set(vecdraw.vec3D{i}.plot,'XData',[0 vectors(i,1)]);
    set(vecdraw.vec3D{i}.plot,'YData',[0 vectors(i,2)]);
    set(vecdraw.vec3D{i}.plot,'ZData',[0 vectors(i,3)]);
end