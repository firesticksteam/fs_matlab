% sets 3D vectors in vectorDrawing
function setDirPointers(dirdraw,angles)

x=cos(angles(1));
y=sin(angles(1));
set(dirdraw.pointer(1),'XData',[-x x],'YData',[-y y]);

x=cos(angles(2));
y=sin(angles(2));
set(dirdraw.pointer(2),'XData',[-x x],'YData',[-y y]);

drawnow;

end
