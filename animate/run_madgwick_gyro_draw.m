% measures madgwick and gyro and displays directions
addpath([pwd '\..\lib']);
dirdraw=createDirectionDrawing();
start_madgwick_gyro;
while exist('running','dir')
    meas_data=fread(stickPort,[COLUMNS,100],FORMAT)';
    % get last quternion
    q=meas_data(end,1:4);
    % calculate rotated x-axis
    vx=rotateVec([1 0 0],q);
    % angle to north
    alpha=atan2(vx(1),vx(2));
    beta=atan2(vx(3),sqrt(vx(1)^2+vx(2)^2));
    setDirPointers(dirdraw,[alpha beta]);
    disp([vx alpha*180/pi]);
end
% stop output
fprintf(stickPort,'stop');




