% show magnet data on osci


% connect to stick
connect2stick;


% configure oscisettings
% titles for subplots, the number is also the number of subplots
os.titles={'Magnitude','Angles'};
% x data number
os.xnum=300;
% plotted lines per subplot
os.dimensions=[1 2];
% y ranges for each subplot
os.yranges=[-3 3;-180 180];



% start measurement
oscidraw=createOsciDrawing(os);
pos=1;
while exist('running','dir')
    % get data
    fprintf(stickPort,'calib_bin 1\n');
    meas_data=fread(stickPort,[9,1],settings.FSFLOAT)';
    magnet_data=meas_data(7:9);
    % calculate magnitude
    magnitude=norm(magnet_data);
    % calculate angles
    heading=atan2(magnet_data(2),magnet_data(1))*180/pi;
    dip=atan2(magnet_data(3),norm(magnet_data(1:2)))*180/pi;
    % set osci data    
    setOsciData(oscidraw,pos,[magnitude heading dip])
    
    pos=pos+1;
    if (pos>os.xnum) 
        pos=1;
    end
    drawnow;
end




