% get serial data from stick and animate it

clear all;
global mperiod;

% the serial port
SERIALPORT='COM4';
% the timer function
TIMERFCN='testfcn1';

mperiod=[];
% create the drawing
createStickDrawing;
% open serial port
% port=serial(SERIALPORT);
% fopen(port);
% wait
% start measurement
% start timer
timer1 = timer('TimerFcn',TIMERFCN, 'Period', 0.001,'ExecutionMode','fixedRate');
tic;
start(timer1);