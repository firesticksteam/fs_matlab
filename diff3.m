function vel=diff3(pos);
% function vel=diff3(pos);
% 3-point differentiation of vector pos
d=[1 0 -1]/2;
vel=conv(pos,d);
% cut first and last rows
vel=vel(2:end-1);
