function x = qr_solve ( m, n, a, b )

%*****************************************************************************80
%
%% QR_SOLVE solves a linear system in the least squares sense.
%
%  Discussion:
%
%    If the matrix A has full column rank, then the solution X should be the
%    unique vector that minimizes the Euclidean norm of the residual.
%
%    If the matrix A does not have full column rank, then the solution is
%    not unique; the vector X will minimize the residual norm, but so will
%    various other vectors.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    27 August 2016
%
%  Author:
%
%    John Burkardt
%
%  Reference:
%
%    David Kahaner, Cleve Moler, Steven Nash,
%    Numerical Methods and Software,
%    Prentice Hall, 1989,
%    ISBN: 0-13-627258-4,
%    LC: TA345.K34.
%
%  Parameters:
%
%    Input, integer M, the number of rows of A.
%
%    Input, integer N, the number of columns of A.
%
%    Input, real A(M,N), the matrix.
%
%    Input, real B(M), the right hand side.
%
%    Output, real X(N), the least squares solution.
%
  lda = m;

  tol = eps / max ( max ( abs ( a(1:m,1:n) ) ) ); 
%
%  Factor the matrix.
%
  [ kr, jpvt, qraux, a ] = dqrank ( a, lda, m, n, tol );
%
%  Solve the least-squares problem.
%
  [ x, r ] = dqrlss ( a, lda, m, n, kr, b, jpvt, qraux );

  return
end

