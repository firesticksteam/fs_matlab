function backslash_solve_test ( )

%*****************************************************************************80
%
%% BACKSLASH_SOLVE_TEST tests MATLAB's backslash operator.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    27 August 2016
%
%  Author:
%
%    John Burkardt
%
  addpath ( '../test_ls' );

  fprintf ( 1, '\n' );
  fprintf ( 1, 'BACKSLASH_SOLVE_TEST\n' );
  fprintf ( 1, '  MATLAB''s backslash operator x=A\\b\n' );
  fprintf ( 1, '  solves a linear system A*x = b in the least squares sense.\n' );
  fprintf ( 1, '  Compare a tabulated solution X1 to the backslash result X2.\n' );

  prob_num = p00_prob_num ( );

  fprintf ( 1, '\n' );
  fprintf ( 1, '  Number of problems = %d\n', prob_num );
  fprintf ( 1, '\n' );
  fprintf ( 1, '  Index     M     N         ||B||   ||X1 - X2||' );
  fprintf ( 1, '        ||X1||        ||X2||        ||R1||        ||R2||\n' );
  fprintf ( 1, '\n' );

  for prob = 1 : prob_num
%
%  Get problem size.
%
    m = p00_m ( prob );
    n = p00_n ( prob );
%
%  Retrieve problem data.
%
    a = p00_a ( prob, m, n );
    b = p00_b ( prob, m );
    x1 = p00_x ( prob, n );

    b_norm = norm ( b );
    x1_norm = norm ( x1 );
    r1 = a * x1 - b;
    r1_norm = norm ( r1 );
%
%  Use MATLAB's backslash operator to solve the system.
%
    x2 = a \ b;
    x2_norm = norm ( x2 );
    r2 = a * x2 - b;
    r2_norm = norm ( r2 );
%
%  Compare tabulated and computed solutions.
%
    x_diff_norm = norm ( x1 - x2 );
%
%  Report results for this problem.
%
    fprintf ( 1, '  %5d  %4d  %4d', prob, m, n );
    fprintf ( 1, '  %12.4g  %12.4g', b_norm, x_diff_norm ); 
    fprintf ( 1, '  %12.4g  %12.4g', x1_norm, x2_norm );
    fprintf ( 1, '  %12.4g  %12.4g\n', r1_norm, r2_norm );

  end

  rmpath ( '../test_ls' );

  return
end

