function qr_solve_tests ( )

%*****************************************************************************80
%
%% QR_SOLVE_TESTS tests the QR_SOLVE library.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    27 August 2016
%
%  Author:
%
%    John Burkardt
%
  timestamp ( );
  fprintf ( 1, '\n' );
  fprintf ( 1, 'QR_SOLVE_TESTS\n' );
  fprintf ( 1, '  MATLAB version\n' );
  fprintf ( 1, '  Test the QR_SOLVE library.\n' );
  fprintf ( 1, '  This test also needs the TEST_LS library.\n' );

  backslash_solve_test ( );
  normal_solve_test ( );
  qr_solve_test ( );
  svd_solve_test ( );
%
%  Terminate.
%
  fprintf ( 1, '\n' );
  fprintf ( 1, 'QR_SOLVE_TESTS\n' );
  fprintf ( 1, '  Normal end of execution.\n' );
  fprintf ( 1, '\n' );
  timestamp ( );

  return
end

