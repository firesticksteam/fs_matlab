function normal_solve_test ( )

%*****************************************************************************80
%
%% NORMAL_SOLVE_TEST tests NORMAL_SOLVE.
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    26 August 2016
%
%  Author:
%
%    John Burkardt
%
  addpath ( '../test_ls' );

  fprintf ( 1, '\n' );
  fprintf ( 1, 'NORMAL_SOLVE_TEST\n' );
  fprintf ( 1, '  NORMAL_SOLVE is a function with a simple interface which\n' );
  fprintf ( 1, '  solves a linear system A*x = b in the least squares sense.\n' );
  fprintf ( 1, '  Compare a tabulated solution X1 to the NORMAL_SOLVE result X2.\n' );
  fprintf ( 1, '\n' );
  fprintf ( 1, '  NORMAL_SOLVE cannot be applied when N < M,\n' );
  fprintf ( 1, '  or if the matrix does not have full column rank.\n' );

  prob_num = p00_prob_num ( );

  fprintf ( 1, '\n' );
  fprintf ( 1, '  Number of problems = %d\n', prob_num );
  fprintf ( 1, '\n' );
  fprintf ( 1, '  Index     M     N         ||B||   ||X1 - X2||' );
  fprintf ( 1, '        ||X1||        ||X2||        ||R1||        ||R2||\n' );
  fprintf ( 1, '\n' );

  for prob = 1 : prob_num
%
%  Get problem size.
%
    m = p00_m ( prob );
    n = p00_n ( prob );
%
%  Retrieve problem data.
%
    a = p00_a ( prob, m, n );
    b = p00_b ( prob, m );
    x1 = p00_x ( prob, n );

    b_norm = norm ( b );
    x1_norm = norm ( x1 );
    r1 = a * x1 - b;
    r1_norm = norm ( r1 );
%
%  Use NORMAL_SOLVE on the problem.
%
    [ x2, flag ] = normal_solve ( m, n, a, b );

    if ( flag ~= 0 )

      fprintf ( 1, '  %5d  %4d  %4d', prob, m, n );
      fprintf ( 1, '  %12.4g  ------------', b_norm ); 
      fprintf ( 1, '  %12.4g  ------------', x1_norm );
      fprintf ( 1, '  %12.4g  ------------\n', r1_norm );

    else

      x2_norm = norm ( x2 );
      r2 = a * x2 - b;
      r2_norm = norm ( r2 );
%
%  Compare tabulated and computed solutions.
%
      x_diff_norm = norm ( x1 - x2 );
%
%  Report results for this problem.
%
      fprintf ( 1, '  %5d  %4d  %4d', prob, m, n );
      fprintf ( 1, '  %12.4g  %12.4g', b_norm, x_diff_norm ); 
      fprintf ( 1, '  %12.4g  %12.4g', x1_norm, x2_norm );
      fprintf ( 1, '  %12.4g  %12.4g\n', r1_norm, r2_norm );

    end

  end

  rmpath ( '../test_ls' );

  return
end

