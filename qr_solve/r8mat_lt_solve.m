function x = r8mat_lt_solve ( n, a, b )

%*****************************************************************************80
%
%% R8MAT_LT_SOLVE solves a transposed lower triangular linear system.
%
%  Discussion:
%
%    Given the lower triangular matrix A, the linear system to be solved is:
%
%      A' * x = b
%
%  Licensing:
%
%    This code is distributed under the GNU LGPL license.
%
%  Modified:
%
%    26 August 2016
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, integer N, the number of rows and columns of the matrix A.
%
%    Input, real A(N,N), the N by N lower triangular matrix.
%
%    Input, real B(N,1), the right hand side of the linear system.
%
%    Output, real X(N,1), the solution of the linear system.
%
  x(1:n,1) = 0.0;
%
%  Solve L'*x = b.
%
  for i = n : -1 : 1
    x(i,1) = ( b(i,1) - a(i+1:n,i)' * x(i+1:n,1) ) / a(i,i);
  end

  return
end
