addpath('quaternion_library'); 

% Import and plot sensor data

convertdata;

figure('Name', 'Sensor Data');
axis(1) = subplot(3,1,1);
hold on;
plot(time, Gyroscope(:,1), 'r');
plot(time, Gyroscope(:,2), 'g');
plot(time, Gyroscope(:,3), 'b');
legend('X', 'Y', 'Z');
xlabel('Time (s)');
ylabel('Angular rate (rad/s)');
title('Gyroscope');
hold off;
axis(2) = subplot(3,1,2);
hold on;
plot(time, Accelerometer(:,1), 'r');
plot(time, Accelerometer(:,2), 'g');
plot(time, Accelerometer(:,3), 'b');
legend('X', 'Y', 'Z');
xlabel('Time (s)');
ylabel('Acceleration (g)');
title('Accelerometer');
hold off;
axis(3) = subplot(3,1,3);
hold on;
plot(time, Magnetometer(:,1), 'r');
plot(time, Magnetometer(:,2), 'g');
plot(time, Magnetometer(:,3), 'b');
legend('X', 'Y', 'Z');
xlabel('Time (s)');
ylabel('Flux (G)');
title('Magnetometer');
hold off;
linkaxes(axis, 'x');

% define start estimations
% orientation quaternion
SEq=[1 0 0 0];
% reference direction of flux in earth frame
b=[1 0 0];
% estimate gyroscope biases error
w_b=[0 0 0];

% get number of steps
framecount=size(data,1);
position=[];
% do the madgwick filtering with all data
for i=1:framecount
    [SEq,b,w_b]=madgwickstep(SEq,b,w_b,Accelerometer(i,:),Gyroscope(i,:),Magnetometer(i,:));
    position=[position;SEq];
end

% Plot algorithm output as quaterion

figure('Name', 'quaterion');
plot(time, position);
title('quaterion');
xlabel('Time (s)');
ylabel('quat');
legend('0', 'x', 'y','z');



%% Plot algorithm output as Euler angles
% The first and third Euler angles in the sequence (phi and psi) become
% unreliable when the middle angles of the sequence (theta) approaches �90
% degrees. This problem commonly referred to as Gimbal Lock.
% See: http://en.wikipedia.org/wiki/Gimbal_lock
euler = quatern2euler(quaternConj(position)) * (180/pi);	% use conjugate for sensor frame relative to Earth and convert to degrees.

figure('Name', 'Euler Angles');
hold on;
plot(time, euler(:,1), 'r');
plot(time, euler(:,2), 'g');
plot(time, euler(:,3), 'b');
title('Euler angles');
xlabel('Time (s)');
ylabel('Angle (deg)');
legend('\phi', '\theta', '\psi');
hold off;

% save data
[pathstr, name, ext] = fileparts(rawdatafile);
savename=fullfile(pathstr, [name '.mat']);
save(savename);

