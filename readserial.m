% include common definitions
defines;
% close open serial ports
closeAllSerialPorts
% open serial conection
serport = serial(comport,'InputBufferSize',4096);
fopen(serport);
fprintf(serport,'stop');
% clear buffer
fclose(serport);
% re-open
fopen(serport);
fprintf(serport,'calib_bin');
% read from serial port
while 1
    b=fread(serport,10,'float32');
    disp(b');
end
