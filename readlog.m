% reads log file 
% needs some folder definitions:
% MEASDIR : local folder where measurement data is stored



% number of columns to be read
COLUMNS=11;
% data format to be read
FORMAT='int16';
% scaling for accel data to g (full range +-16g)
ACCELSCALE=1/2048;
% scaling for gyro data to 1/s (rounds per second) full range +-2000dps
GYROSCALE=1/5904;
% scaling for mag data to G (16 bit measurement, 1LSB=0.15�T)
MAGSCALE=0.0015;




% set local folders
folders=local_folders();
if exist('filename')
    fileID=fopen([folders.MEASDIR '\\' filename],'raw data');
else    
    % base directory
    basedir=[folders.MEASDIR '\\*.*'];
    [filename,pathname]=uigetfile(basedir);
    fileID = fopen([pathname filename]);
end
clear filename;
data=fread(fileID,[COLUMNS,inf],FORMAT)';
fclose(fileID);




spiTime=data(:,11);
accel=data(:,1:3)*ACCELSCALE;
accelnorm=sqrt(sum(accel.^2,2));
temperature=data(:,4);
gyro=data(:,5:7)*GYROSCALE;
mag=data(:,8:10)*MAGSCALE;