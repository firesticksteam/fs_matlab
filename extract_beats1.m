% extract beats from data
% measured data: madgwick and calib

% velocity threshold for triggering signal
VELTHRESHUP=3;
VELTHRESHDOWN=3;
% low pass filter frequency
LPFREQU=10;

madgwick_data=data(:,1:4);
accel_data=data(:,5:7);
gyro_data=data(:,8:10);
mag_data=data(:,11:13);

% for further processing, get a one dimensional sinal; here gyroy
velocity=gyro_data(:,2);
% filter the signal
[b,a]=butter(2,LPFREQU/500);
velocity_tp=filter(b,a,velocity);


maxvel=0;
maxveli=0;
triggerpoints=[];
lastv=0;
searchmin=0;
% step by step extraction
ds=size(data,1);
for i=1:ds
    v=velocity_tp(i);
    if (v>VELTHRESHUP) && (v>lastv)
        maxvel=v;
        maxveli=i;
    end
    if(maxvel~=0) && (v<VELTHRESHDOWN) && (v>lastv)
        % this is the trigger point
        triggerpoints=[triggerpoints;maxveli,maxvel,i,v];
        % enable new measurement
        maxvel=0;
        maxveli=0;
    end  
    lastv=v;
end

% plot it
close all;
plot(velocity_tp);
hold on;
for i=1:size(triggerpoints,1)
    t=triggerpoints(i,:);
    plot([t(1) t(1)],[t(2)-1 t(2)+1],'r');
    plot([t(3) t(3)],[t(4)-1 t(4)+1],'g');
end
hold off;

