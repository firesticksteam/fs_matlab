function data=record_calib(period,sampletime)

global stickPort;

% sample number
samplenum=1000/period*sampletime;

% connect to stick
connect2stick;

% set timeout
set(stickPort,'Timeout',sampletime*1.5);

% configure period
fprintf(stickPort,['measperiod ' num2str(period) '\n']);
% get data
fprintf(stickPort,['calib_bin ' num2str(samplenum) '\n']);
data=fread(stickPort,[9,samplenum],settings.FSFLOAT)';
% reset period
fprintf(stickPort,'measperiod 1');
% reset timeout
set(stickPort,'Timeout',10);
